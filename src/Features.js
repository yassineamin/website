import React, { Component } from "react";
 
class Features extends Component {
  render() {
    return (
<div>
<div id="app-features" class="section">
      <div class="container">
        <div class="section-header">   
          <p class="btn btn-subtitle wow fadeInDown" data-wow-delay="0.2s">features</p>       
          <h2 class="section-title wow fadeIn" data-wow-delay="0.2s">Amazing Features</h2>
        </div>
        <div class="row">
          <div class="col-lg-4 col-md-12 col-xs-12">
            <div class="content-left text-right">
              <div class="box-item left">
                <span class="icon">
                  <i class="lni-leaf"></i>
                </span>
                <div class="text">
                  <h4>User Friendly</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing typesting industry text.</p>
                </div>
              </div>
              <div class="box-item left">
                <span class="icon">
                  <i class="lni-dashboard"></i>
                </span>
                <div class="text">
                  <h4>Super Fast Speed</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing typesting industry text.</p>
                </div>
              </div>
              <div class="box-item left">
                <span class="icon">
                  <i class="lni-headphone-alt"></i>
                </span>
                <div class="text">
                  <h4>24/7 Support</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing typesting industry text.</p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-12 col-xs-12">
            <div class="show-box">
              <img src="img/features/app.png" alt=""/>
            </div>
          </div>
          <div class="col-lg-4 col-md-12 col-xs-12">
            <div class="content-right text-left">
              <div class="box-item right">
                <span class="icon">
                  <i class="lni-shield"></i>
                </span>
                <div class="text">
                  <h4>Secure</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing typesting industry text.</p>
                </div>
              </div>
              <div class="box-item right">
                <span class="icon">
                  <i class="lni-star-filled"></i>
                </span>
                <div class="text">
                  <h4>Awesome Rating</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing typesting industry text.</p>
                </div>
              </div>
              <div class="box-item right">
                <span class="icon">
                  <i class="lni-cup"></i>
                </span>
                <div class="text">
                  <h4>Award Winning</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing typesting industry text.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <section class="video-promo section">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12">
              <div class="video-promo-content text-center">
                <a href="https://www.youtube.com/watch?v=yP6kdOZHids" class="video-popup"><i class="lni-film-play"></i></a>
                <h2 class="mt-3 wow zoomIn" data-wow-duration="1000ms" data-wow-delay="100ms">Watch Video</h2>
              </div>
          </div>
        </div>
      </div>
    </section>

    <div id="features-two" class="section">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 col-md-12 col-sm-12">
            <div class="img-thumb wow fadeInLeft" data-wow-delay="0.3s">
              <img class="img-fluid" src="img/features/img1.png" alt=""/>
            </div>
          </div>
          <div class="col-lg-6 col-md-12 col-sm-12">
            <div class="text-wrapper wow fadeInRight" data-wow-delay="0.6s">
              <div>
                <p class="btn btn-subtitle">How It Works?</p>  
                <h3>Our App Is Amazing!</h3>
                <p>Appropriately implement one-to-one catalysts for change vis-a-vis wireless catalysts for change. Enthusiastically architect adaptive e-tailers after sustainable total linkage.</p>
                <a class="btn btn-rm" href="#">Read More <i class="lni-arrow-right"></i></a>
              </div>
            </div>
          </div>
        </div>
        <div class="row mt">
          <div class="col-lg-6 col-md-12 col-sm-12">
            <div class="text-wrapper wow fadeInRight" data-wow-delay="0.9s">
              <div>
                <p class="btn btn-subtitle">Update</p>  
                <h3>Free Updates!</h3>
                <p>Appropriately implement one-to-one catalysts for change vis-a-vis wireless catalysts for change. Enthusiastically architect adaptive e-tailers after sustainable total linkage.</p>
                <a class="btn btn-rm" href="#">Read More <i class="lni-arrow-right"></i></a>
              </div>
            </div>
          </div>
           <div class="col-lg-6 col-md-12 col-sm-12">
            <div class="img-thumb wow fadeInLeft" data-wow-delay="1.2s">
              <img class="img-fluid" src="img/features/img2.png" alt=""/>
            </div>
          </div>
        </div>
      </div>
    </div>



    <footer>
      <section class="footer-Content">
        <div class="container">
          <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12">
              <img src="img/logo.png" alt=""/>
              <div class="textwidget">
                <p>Appropriately implement one-to-one catalysts for change vis-a-vis wireless catalysts for change. Enthusiastically architect adaptive.</p>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12">
              <div class="widget">
                <h3 class="block-title">Create a Free Account</h3>
                <ul class="menu">
                  <li><a href="#">Sign In</a></li>
                  <li><a href="#">About Us</a></li>
                  <li><a href="#">Pricing</a></li>
                  <li><a href="#">Jobs</a></li>
                </ul>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12">
              <div class="widget">
                <h3 class="block-title">Resource</h3>
                <ul class="menu">
                  <li><a href="#">Comunnity</a></li>
                  <li><a href="#">Become a Partner</a></li>
                  <li><a href="#">Our Technology</a></li>
                  <li><a href="#">Documentation</a></li>
                </ul>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12">
              <div class="widget">
                <h3 class="block-title">Support</h3>
                <ul class="menu">
                  <li><a href="#">Terms & Condition</a></li>
                  <li><a href="#">Contact Us</a></li>
                  <li><a href="#">Privacy Policy</a></li>
                  <li><a href="#">Help</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="copyright">
          <div class="container">
            <div class="row">
              <div class="col-md-12">
                <div class="site-info float-left">
                  <p>&copy; 2020 - Designed by <a href="http://uideck.com" rel="nofollow">UIdeck</a></p>
                </div>              
                <div class="float-right">  
                  <ul class="footer-social">
                    <li><a class="facebook" href="#"><i class="lni-facebook-filled"></i></a></li>
                    <li><a class="twitter" href="#"><i class="lni-twitter-filled"></i></a></li>
                    <li><a class="linkedin" href="#"><i class="lni-linkedin-fill"></i></a></li>
                    <li><a class="google-plus" href="#"><i class="lni-google-plus"></i></a></li>
                  </ul> 
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      
    </footer>

</div>
    );
  }
}
 
export default Features;