import React, { Component } from "react";
import {
  Route,
  NavLink,
  HashRouter
} from "react-router-dom";
import Home from "./Home";
import Features from "./Features"; 
import Contact from "./Contact"; 

class Main extends Component {
    render() {
      return (
<HashRouter>
<header id="home" class="hero-area-2">        
  <div class="overlay"></div>
  <nav class="navbar navbar-expand-md bg-inverse fixed-top scrolling-navbar">
    <div class="container">
      <a href="index.html" class="navbar-brand"><img src="img/logo.png" alt=""/></a>  
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <i class="lni-menu"></i>
      </button>

      <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto w-100 justify-content-end">
          <li class="nav-item"><NavLink class="nav-link page-scroll" to="/">Home</NavLink></li>
          <li class="nav-item"><NavLink class="nav-link page-scroll" to="/features">Features</NavLink></li>
          <li class="nav-item"><NavLink class="nav-link page-scroll" to="/stuff">Screenshots</NavLink></li>
          <li class="nav-item"><NavLink class="nav-link page-scroll" to="/stuff">Testimonial</NavLink></li>
          <li class="nav-item"><NavLink class="nav-link page-scroll" to="/stuff">Plans</NavLink></li>
          <li class="nav-item"><NavLink class="nav-link page-scroll" to="/stuff">Download</NavLink></li>                  
          <li class="nav-item"><NavLink class="nav-link page-scroll" to="/contact">Contact</NavLink></li>
          
        </ul>
      </div>
    </div>
  </nav>
</header>



    
<div className="content">
  <Route exact path="/" component={Home}/>
  <Route path="/features" component={Features}/>
  <Route path="/contact" component={Contact}/>
</div>
</HashRouter>
      );
    }
  }
  export default Main;